#include <stdio.h>
int main()
{
    int a[5][3],i,j,highest_marks;
    for(i=0;i<5;i++)
    {
        printf("\nEnter the marks of student %d",i);
        for(j=0;j<3;j++)
        {
            printf("\nMarks in subject a[%d][%d]=",i,j);
            scanf("%d",&a[i][j]);
        }
    }
    for(j=0;j<3;j++)
    {
        highest_marks=a[0][j];
        for(i=1;i<5;i++)
        {
            if(a[i][j]>highest_marks)
            highest_marks=a[i][j];
        }
        printf("\nThe highest marks in %d subject = %d\n",j,highest_marks);
    }
    return 0;
}